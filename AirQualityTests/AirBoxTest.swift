//
//  AirBoxTest.swift
//  AirQualityTests
//
//  Created by 吳成恩 on 2018/7/6.
//  Copyright © 2018年 吳成恩. All rights reserved.
//

import XCTest
@testable import AirQuality

class AirBoxTest: XCTestCase {
    var jsonData = [String: Any]()
    var rawData = Data()
    
    var quality1 = AirQuality(pm25: 10)
    var quality2 = AirQuality(pm25: 60)
    var quality3 = AirQuality(pm25: 120)
    var quality4 = AirQuality(pm25: 160)
    var quality5 = AirQuality(pm25: 220)
    var quality6 = AirQuality(pm25: 320)
    var qualityUnknown = AirQuality(pm25: 9999)
    
    override func setUp() {
        if let path = Bundle.main.path(forResource: "AirBoxRawData", ofType: "json") {
            do {
                rawData = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                jsonData = try JSONSerialization.jsonObject(with: rawData, options: .mutableLeaves) as! [String : Any]
                
            }
            catch {
                XCTFail(error.localizedDescription)
            // handle error
            }
        }
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    func testParseCodablePerformance() {
        self.measure {
            for rawObject in jsonData.values{
                do{
                    let jsonData = try JSONSerialization.data(withJSONObject: rawObject, options: .prettyPrinted)
                    let decoder = JSONDecoder()
                    _ = try decoder.decode(AirBox.self, from: jsonData)
                }
                catch let error {
                    print("parse error: \(error)")
                }
            }
        }
    }
    /*
    func testParseManualPerformance() {
        self.measure {
            for rawObject in jsonData{
                _ = AirBox(json: rawObject.value as! [String : Any])
            }
        }
    }
    */
    func testQualityLevel() {

        XCTAssert(quality1.range == AirQuality.Range.level1)
        XCTAssert(quality2.range == AirQuality.Range.level2)
        XCTAssert(quality3.range == AirQuality.Range.level3)
        XCTAssert(quality4.range == AirQuality.Range.level4)
        XCTAssert(quality5.range == AirQuality.Range.level5)
        XCTAssert(quality6.range == AirQuality.Range.level6)
        XCTAssert(qualityUnknown.range == AirQuality.Range.unknown)
    }
    func testQualityStatus() {


        XCTAssert(quality1.status.count != 0)
        XCTAssert(quality2.status.count != 0)
        XCTAssert(quality3.status.count != 0)
        XCTAssert(quality4.status.count != 0)
        XCTAssert(quality5.status.count != 0)
        XCTAssert(quality6.status.count != 0)
        XCTAssert(qualityUnknown.status.count != 0)
    }
    
    func testQualitySuggestion() {
        XCTAssert(quality1.suggestion.activity.count != 0)
        XCTAssert(quality2.suggestion.activity.count != 0)
        XCTAssert(quality3.suggestion.activity.count != 0)
        XCTAssert(quality4.suggestion.activity.count != 0)
        XCTAssert(quality5.suggestion.activity.count != 0)
        XCTAssert(quality6.suggestion.activity.count != 0)
        XCTAssert(qualityUnknown.suggestion.activity.count != 0)
        
        XCTAssert(quality1.suggestion.health.count != 0)
        XCTAssert(quality2.suggestion.health.count != 0)
        XCTAssert(quality3.suggestion.health.count != 0)
        XCTAssert(quality4.suggestion.health.count != 0)
        XCTAssert(quality5.suggestion.health.count != 0)
        XCTAssert(quality6.suggestion.health.count != 0)
        XCTAssert(qualityUnknown.suggestion.health.count != 0)
    }
    
}
