//
//  TabBarViewController.swift
//  AirQuality
//
//  Created by 吳成恩 on 2018/7/17.
//  Copyright © 2018年 吳成恩. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire

private let kExpireTime = TimeInterval(60.0)

class TabBarViewController: UITabBarController {
    let locationManager = CLLocationManager()
    var lastLocation: CLLocation? = nil
    var previousSelectedIndex = 0
    
    lazy var listVC: ListViewController = {
        let naviVC = viewControllers?.first! as! UINavigationController
        let vc = naviVC.topViewController! as! ListViewController
        vc.delegate = self
        return vc
    }()
    lazy var mapVC: MapViewController = {
        return viewControllers?.last! as! MapViewController
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        print(#function)
        initVariable()
        initLayout()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
        case .denied, .restricted:
            showLocationAuthorizationAlert()
        default:
            locationManager.requestLocation()
        }
        fetchingAirBoxDataFromLocal()
    }
    //MARK: Private
    private func initVariable() {
        delegate = self
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        lastLocation = locationManager.location
    }
    private func initLayout() {
        listVC.loadViewIfNeeded()
        mapVC.loadViewIfNeeded()
    }
    private func showLocationAuthorizationAlert() {
        let alertVC = UIAlertController(title: "系統訊息", message: "尚未打開位置授權，是否前往設定？", preferredStyle: .alert)
        let goAction = UIAlertAction(title: "前往", style: .default) { (action) in
            if let settingsURL = URL(string: UIApplication.openSettingsURLString + Bundle.main.bundleIdentifier!) {
                UIApplication.shared.open(settingsURL, completionHandler: { (success) in
                    print(success)
                })
            }
        }
        let cancelAction = UIAlertAction(title: "取消", style: .cancel)
        alertVC.addAction(goAction)
        alertVC.addAction(cancelAction)
        self.present(alertVC, animated: true)
    }
    private func updateServerData(airBoxes: [AirBox]) {
        DispatchQueue.main.async {
            self.listVC.updateServerData(airBoxes: airBoxes)
            self.mapVC.updateServerData(airBoxes: airBoxes)
        }
    }
    private func fetchingAirBoxDataFromLocal() {
        switch AirQualityManager.shared.savedAirBox() {
        case .notFound:
            fetchingAirBoxDataFromServer()
        case .find(var data, let expired):
            if let location = locationManager.location {
                AirQualityManager.shared.sortNearest(boxes: &data, with: location)
            }
            self.updateServerData(airBoxes: data)
            if expired{
                listVC.listTableView.refreshControl?.beginRefreshing()
                fetchingAirBoxDataFromServer()
            }
        }
    }
    private func fetchingAirBoxDataFromServer() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        AirQualityManager.shared.fetchAirBoxData(sortWith: locationManager.location, needToSave: true) { (result) in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            switch result {
            case .success(let data):
                self.updateServerData(airBoxes: data)
            case .failure(let error):
                print(error)
            }
        }
    }
}
extension TabBarViewController: UITabBarControllerDelegate{
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        
        if selectedIndex == previousSelectedIndex {
            switch selectedIndex {
            case 0:
                listVC.listTableView.setContentOffset(.zero, animated: true)
            case 1:
                guard let userLocation = locationManager.location else { return }
                mapVC.lookMap(at: userLocation.coordinate, animated: true)
            default: break
            }
        }
        previousSelectedIndex = selectedIndex
    }
}
extension TabBarViewController: CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else { return }
        if lastLocation == nil {
            lastLocation = location
        }
        else if var lastLocation = lastLocation, lastLocation.distance(from: location) > 100.0 {
            fetchingAirBoxDataFromLocal()
            lastLocation = location
        }
    }
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
}

extension TabBarViewController: ListViewControllerDelegate{
    func listViewController(didSelect box: AirBox) {
        selectedIndex = 1
        mapVC.lookMap(with: box)
    }
    func listViewControllerTriggleRefresh() {
        fetchingAirBoxDataFromServer()
    }
}
