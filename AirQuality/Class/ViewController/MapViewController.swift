//
//  MapViewController.swift
//  AirQuality
//
//  Created by 吳成恩 on 2018/7/17.
//  Copyright © 2018年 吳成恩. All rights reserved.
//

import UIKit
import MapKit
class MapViewController: UIViewController {
    @IBOutlet weak var appleMap: MKMapView!
    var isFirstTime: Bool = true
    var allAnnontations = [MKAnnotation]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initVariable()
        initLayout()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isFirstTime {
            isFirstTime = false
            lookMap(at: appleMap.userLocation.coordinate, animated: false)
        }
    }
    private func initVariable() {
        appleMap.showsUserLocation = true
        appleMap.register(AirBoxAnnotationView.self, forAnnotationViewWithReuseIdentifier: "AirBoxAnnotationView")
    }
    private func initLayout() {
    }
    // MARK: Public
    func updateServerData(airBoxes: [AirBox]) {
        allAnnontations = airBoxes.map { (box) -> AirBoxAnnotation in
            let annotation = AirBoxAnnotation(with: box)
            return annotation
        }
        self.appleMap.removeAnnotations(self.appleMap.annotations)
        self.appleMap.addAnnotations(self.allAnnontations)
    }
    func lookMap(with box: AirBox){
        let filteredAnnotations = allAnnontations.filter { $0.title == box.name }
        if let annotation = filteredAnnotations.first {
            lookMap(at: annotation.coordinate, animated: false)
        }
    }
    func lookMap(at coordinate: CLLocationCoordinate2D, animated: Bool){
        let camera = MKMapCamera(lookingAtCenter: coordinate, fromDistance: 1000, pitch: 0.0, heading: 0)
        appleMap.setCamera(camera, animated: animated)
    }
}

extension MapViewController: MKMapViewDelegate{
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        var beRetainAnnotations = [MKAnnotation]()
        var beRemovedAnnotations = [MKAnnotation]()
        
        for annotation in mapView.annotations {
            if mapView.visibleMapRect.contains(MKMapPoint(annotation.coordinate)){
                beRetainAnnotations.append(annotation)
            }
            else{
                beRemovedAnnotations.append(annotation)
            }
        }
        
        let beAddedAnnotations = allAnnontations.filter { (annotation) -> Bool in
            return mapView.visibleMapRect.contains(MKMapPoint(annotation.coordinate)) && !beRetainAnnotations.contains{$0 == annotation}
        }
        appleMap.removeAnnotations(beRemovedAnnotations)
        appleMap.addAnnotations(beAddedAnnotations)
    }
 
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let annotation = annotation as? AirBoxAnnotation else { return nil }
        let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "AirBoxAnnotationView", for: annotation)
        annotationView.annotation = annotation
        return annotationView
    }
}

extension MKAnnotation{
    static func ==(lhs: Self, rhs: Self) -> Bool {
        return lhs.hash == rhs.hash
    }
}
