//
//  BaseViewController.swift
//  AirQuality
//
//  Created by 吳成恩 on 2018/7/5.
//  Copyright © 2018年 吳成恩. All rights reserved.
//

import UIKit
protocol ListViewControllerDelegate{
    func listViewController(didSelect box:AirBox)
    func listViewControllerTriggleRefresh()
}
class ListViewController: UIViewController {
    @IBOutlet weak var listTableView: UITableView!
    var delegate: ListViewControllerDelegate?
    var airBoxesData = [AirBox]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initVariable()
        initLayout()
        // Do any additional setup after loading the view.
    }
    private func initVariable() {
    }
    private func initLayout() {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(handleRefresh(control:)), for: .valueChanged)
        listTableView.refreshControl = refreshControl
        listTableView.refreshControl?.beginRefreshing()
    }
    @objc private func handleRefresh(control: UIRefreshControl) {
        delegate?.listViewControllerTriggleRefresh()
    }
    //MARK: Public
    func setIsLoading(_ isLoading: Bool) {
        if isLoading {
            listTableView.refreshControl?.beginRefreshing()
        }
        else{
            listTableView.refreshControl?.endRefreshing()
        }
    }
    func updateServerData(airBoxes: [AirBox]) {
        airBoxesData = airBoxes
        DispatchQueue.main.async {
            self.listTableView.reloadData()
            self.listTableView.refreshControl?.endRefreshing()
        }
    }
}

extension ListViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AirQualityTableViewCell") as! AirBoxTableViewCell
        cell.conigurate(airBox: &self.airBoxesData[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return airBoxesData.count
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let kCell = cell as? AirBoxTableViewCell {
            kCell.animate()
        }
        let toCenter = cell.center
        cell.center = CGPoint(x: cell.center.x, y: cell.center.y + 20)
        cell.alpha = 0.0
        
        UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseIn, animations: {
            cell.center = toCenter
            cell.alpha = 1.0
        })
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let airBox = self.airBoxesData[indexPath.row]
        delegate?.listViewController(didSelect: airBox)
    }
}

extension ListViewController: UIScrollViewDelegate{
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        /*
        if velocity.y > 0{
            UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseOut, animations: {
                self.navigationController?.setNavigationBarHidden(true, animated: true)
            })
        }
        else{
            UIView.animate(withDuration: 0.3, delay: 0.0, options: .curveEaseIn, animations: {
                self.navigationController?.setNavigationBarHidden(false, animated: true)
            })
        }
         */
    }
}
