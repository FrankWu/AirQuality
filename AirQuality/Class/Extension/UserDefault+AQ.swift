//
//  UserDefault+AQ.swift
//  AirQuality
//
//  Created by 吳成恩 on 2018/7/23.
//  Copyright © 2018年 吳成恩. All rights reserved.
//

import Foundation

private let kSavedKey = "kAirQualityData"
private let kSavedDate = "kAirQualitySavedDate"
private let kSharedGroup = "group.shacur.AirQuality"

extension UserDefaults{
    typealias Saved = (data: [AirBox], timeStamp: Date)
    
    static var main: UserDefaults = {
        let userDefault = UserDefaults(suiteName: kSharedGroup)!
        return userDefault
    }()
    
    static func save(_ boxData: [AirBox]) {
        autoreleasepool {
            do {
                let encodedData = try JSONEncoder().encode(boxData)
                UserDefaults.main.set(encodedData, forKey: kSavedKey)
                UserDefaults.main.set(Date(), forKey: kSavedDate)
            }
            catch {
                print(error)
            }
        }
    }
    static func getAirQualityData() -> Saved? {
        guard let savedData = UserDefaults.main.object(forKey: kSavedKey) as? Data  else { return nil }
        do {
            let decodedData = try JSONDecoder().decode([AirBox].self, from: savedData)
            guard let savedDate = UserDefaults.main.object(forKey: kSavedDate) as? Date else { return nil }
            return (decodedData, savedDate)
        }
        catch {
            print(error)
        }
        return nil
    }
}
