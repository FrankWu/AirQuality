//
//  AmountView.swift
//  AirQuality
//
//  Created by 吳成恩 on 2018/7/24.
//  Copyright © 2018年 吳成恩. All rights reserved.
//

import UIKit
private let kAnimationDelay = 0.3
private let kAnimationDuration = 1.0

class AmountView: UIView {
    let amountLayer: CAShapeLayer
    let amountLabel: UILabel
    var presentColor: UIColor?
    var value: Double = 0.0
    
    required init?(coder aDecoder: NSCoder) {
        amountLayer = CAShapeLayer()
        amountLabel = UILabel()
        
        super.init(coder: aDecoder)
        
        setup()

        amountLayer.bounds = bounds
        amountLayer.position = CGPoint(x: 30, y: 45)
        amountLayer.lineCap = .round
        amountLayer.fillColor = UIColor.clear.cgColor
        amountLayer.lineWidth = 5.0
        self.layer.addSublayer(amountLayer)
        
        amountLabel.frame = CGRect(x: 0, y: self.bounds.height - 20, width: self.bounds.width, height: 20)
        amountLabel.font = UIFont(name: "PingFangSC-Medium", size: 18.0)
        amountLabel.textAlignment = .center
        amountLabel.text = "0"
        
        self.addSubview(amountLabel)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        amountLayer.path = path(for: 0)
    }
    private func setup(){
        let bgLayer = CAShapeLayer()
        bgLayer.bounds = bounds
        bgLayer.position = CGPoint(x: 30, y: 45)
        bgLayer.lineCap = .round
        bgLayer.fillColor = UIColor.clear.cgColor
        bgLayer.strokeColor = #colorLiteral(red: 0.8588235294, green: 0.8588235294, blue: 0.8588235294, alpha: 1)
        bgLayer.lineWidth = 6.0
        let circleCenter = CGPoint(x: bounds.size.width / 2, y: bounds.size.height / 2)
        let path = UIBezierPath(arcCenter: circleCenter, radius: 28, startAngle: CGFloat.pi, endAngle: 0, clockwise: true)
        bgLayer.path = path.cgPath
        self.layer.addSublayer(bgLayer)
    }

    private func path(for amount: Int) -> CGPath {
        let percent = CGFloat(amount) / 500.0
        var endAngle = CGFloat.pi + CGFloat.pi * percent;
        if endAngle > CGFloat.pi * 2{
            endAngle = CGFloat.pi * 2;
        }
        let circleCenter = CGPoint(x: bounds.size.width / 2, y: bounds.size.height / 2)
        let path = UIBezierPath(arcCenter: circleCenter, radius: 28, startAngle: CGFloat.pi, endAngle: endAngle, clockwise: true)
        return path.cgPath
    }
    
    //MARK: Public
    func animate() {
        let animationStrokeEnd = CABasicAnimation(keyPath: "strokeEnd")
        animationStrokeEnd.fromValue = 0.0
        animationStrokeEnd.toValue = 1.0
        
        let animationColor = CAKeyframeAnimation(keyPath: "strokeColor")
        animationColor.values = [presentColor!.cgColor]
        
        let animationGroup = CAAnimationGroup()
        animationGroup.timingFunction = CAMediaTimingFunction(name: .easeOut)
        animationGroup.isRemovedOnCompletion = true
        animationGroup.fillMode = .backwards
        animationGroup.duration = CFTimeInterval(kAnimationDuration)
        animationGroup.beginTime = CACurrentMediaTime() + kAnimationDelay
        animationGroup.animations = [animationStrokeEnd, animationColor]
        amountLayer.add(animationGroup, forKey: "Animation")
    }
    func configurate(amount: Int, color: UIColor?) {
        if amount > 500 {
            amountLabel.text = "500+"
        }
        else{
            amountLabel.text = String(amount)
        }
        value = Double(amount)
        
        amountLayer.path = path(for: amount)
        if let color = color {
            presentColor = color
            amountLayer.strokeColor = color.cgColor
        }
    }
}
