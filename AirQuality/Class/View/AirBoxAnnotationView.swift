//
//  AirBoxAnnotationView.swift
//  AirQuality
//
//  Created by 吳成恩 on 2018/7/18.
//  Copyright © 2018年 吳成恩. All rights reserved.
//

import MapKit

class AirBoxAnnotationView: MKMarkerAnnotationView {
    override var annotation: MKAnnotation?{
        didSet{
            guard let kAnnotation = annotation as? AirBoxAnnotation else { return }
            if kAnnotation.data.pm25 > 500.0{
                glyphText = "500+"
                displayPriority = .required
            }
            else{
                displayPriority = .defaultLow
                glyphText = String(Int(kAnnotation.data.pm25))
            }
            markerTintColor = kAnnotation.data.quality.color
        }
    }
}
