
//
//  AirQualityTableViewCell.swift
//  AirQuality
//
//  Created by 吳成恩 on 2018/7/17.
//  Copyright © 2018年 吳成恩. All rights reserved.
//

import UIKit

class AirBoxTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var amountView: AmountView!
    @IBOutlet weak var tempertureLabel: UILabel!
    
    @IBOutlet weak var humidyLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    //MARK: Public
    func conigurate( airBox: inout AirBox) {
        nameLabel.text = airBox.name
        amountView.configurate(amount: Int(airBox.pm25), color: airBox.quality.color)
        timeLabel.text = NSLocalizedString("updateTime", comment: "") + ":" + airBox.formattedTime
        statusLabel.text = airBox.quality.status
        statusLabel.textColor = airBox.quality.color
        tempertureLabel.text = String(Int(airBox.temperature))
        humidyLabel.text = String(Int(airBox.humidity))
    }
    func animate() {
        amountView.animate()
    }
}


