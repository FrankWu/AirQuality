//
//  AirBox.swift
//  AirQuality
//
//  Created by 吳成恩 on 2018/7/5.
//  Copyright © 2018年 吳成恩. All rights reserved.
//

import Foundation
import CoreLocation
import MapKit

struct AirBox: Codable{
    let lng, lat: Double
    let pm25, temperature, humidity: Double
    let id, name, time: String
    
    lazy var cooridnate: CLLocationCoordinate2D = {
        return CLLocationCoordinate2D(latitude: lat, longitude: lng)
    }()
    lazy var quality: AirQuality = {
        return AirQuality(pm25: pm25)
    }()

    lazy var formattedTime: String = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        //formatter.timeZone = TimeZone(secondsFromGMT: 8*24*60*60)
        let originalDate = formatter.date(from: time)
        let toFormatter = DateFormatter()
        toFormatter.dateFormat = "HH:mm"
        return toFormatter.string(from: originalDate!)
    }()

    init(edimax: AirBoxEdimax) {
        lng = edimax.lon
        lat = edimax.lat
        pm25 = Double(edimax.pm25)
        name = edimax.name
        temperature = edimax.t
        humidity = edimax.h
        id = edimax.id
        time = edimax.time
    }
    /*
    init(json: [String: Any]) {
        lng = json["lng"] as? Double ?? 0.0
        lat = json["lat"] as? Double ?? 0.0
        pm25 = json["pm25"] as? Double ?? 0.0
        name = json["name"] as? String ?? ""
        temperature = json["temperature"] as? Double ?? 0.0
        humidity = json["humidity"] as? Double ?? 0.0
        id = json["id"] as? String ?? ""
        time = json["time"] as? String ?? ""
    }
    
    enum CodingKeys: String, CodingKey {
        case latitude = "gps_lat"
        case longitude = "gps_lon"
    }
     */
}


struct AirQuality {
    enum Range: Int {
        case unknown = -1
        case level1 = 1
        case level2
        case level3
        case level4
        case level5
        case level6
        init(pm25: Double){
            switch pm25 {
            case 0...50:
                self = .level1
            case 51...100:
                self = .level2
            case 101...150:
                self = .level3
            case 151...200:
                self = .level4
            case 201...300:
                self = .level5
            case 301...400:
                self = .level6
            default:
                self = .unknown
            }
        }
    }
    struct Suggestion {
        let health: String
        let activity: String
        init (for range: Range){
            health = NSLocalizedString("qualitySuggestionForHealth.level" + String(range.rawValue), comment: "")
            activity = NSLocalizedString("qualitySuggestionForActivity.level" + String(range.rawValue), comment: "")
        }
    }
    let range: Range
    lazy var status: String = {
        return NSLocalizedString("qualityStatus.level" + String(range.rawValue), comment: "")
    }()
    lazy var suggestion: Suggestion = {
        return Suggestion(for: range)
    }()
    lazy var color: UIColor? = {
        return UIColor(named: "Quality.level" + String(range.rawValue))
    }()
    
    init(pm25: Double) {
        range = Range(pm25: pm25)
    }
}

struct AirBoxEdimax: Codable{
    let id, name: String
    let lat, lon: Double
    let pm25, pm10, pm1, co2: Int
    let hcho: Double
    let tvoc, co: Int
    let t, h: Double
    let time, utcTime, org, area: String
    //let type: TypeEnum
    //let odm: Odm
    let status: String
    let adfStatus: Int
    
    enum CodingKeys: String, CodingKey {
        case id, name, lat, lon, pm25, pm10, pm1, co2, hcho, tvoc, co, t, h, time
        case utcTime = "utc_time"
        case org, area, status
        case adfStatus = "adf_status"
    }
}
