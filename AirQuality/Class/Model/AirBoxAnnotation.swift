//
//  AirQualityAnnotation.swift
//  AirQuality
//
//  Created by 吳成恩 on 2018/7/18.
//  Copyright © 2018年 吳成恩. All rights reserved.
//

import UIKit
import MapKit

class AirBoxAnnotation: NSObject, MKAnnotation {
    var coordinate: CLLocationCoordinate2D {
        return data.cooridnate
    }
    var title: String?
    var subtitle: String?
    var data: AirBox
    
    init(with airBox: AirBox) {
        data = airBox
        title = airBox.name
        subtitle = airBox.time
    }
}


