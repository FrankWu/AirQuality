//
//  AirQualityManager.swift
//  AirQuality
//
//  Created by 吳成恩 on 2018/7/27.
//  Copyright © 2018年 吳成恩. All rights reserved.
//

import Foundation
import Alamofire
import CoreLocation

fileprivate let kExpireTime = TimeInterval(600.0)

private var _shareInstance: AirQualityManager!

class AirQualityManager {
    static var shared: AirQualityManager{
        if _shareInstance == nil{
            _shareInstance = AirQualityManager()
        }
        return _shareInstance
    }
    private let apiService: APIProtocol
    
    class func setup(api: APIProtocol) {
        _shareInstance = AirQualityManager(with: api)
    }
    enum DataState {
        case notFound
        case find(data: [AirBox], hasExpired: Bool)
    }
    private init(with api: APIProtocol = APIManager.shared) {
        apiService = api
    }
    //MARK: Private
    private func parseAirBoxEdimax(_ rawData: [[String: Any]]) -> [AirBox] {
        var parsedAirBoxData = [AirBox]()
        for rawObject in rawData{
            autoreleasepool{
                do{
                    let jsonData = try JSONSerialization.data(withJSONObject: rawObject, options: .prettyPrinted)
                    let airBoxEdimax = try JSONDecoder().decode(AirBoxEdimax.self, from: jsonData)
                    guard airBoxEdimax.status == "online" else { return }
                    let airBox = AirBox(edimax: airBoxEdimax)
                    parsedAirBoxData.append(airBox)
                }
                catch let error {
                    print("parse error: \(error)")
                }
            }
        }
        return parsedAirBoxData
    }
    //MARK: Public
    func sortNearest(boxes: inout [AirBox], with location: CLLocation) {
        boxes.sort { (box1, box2) -> Bool in
            let location1 = CLLocation(latitude: box1.lat, longitude: box1.lng)
            let location2 = CLLocation(latitude: box2.lat, longitude: box2.lng)
            if location.distance(from: location1) < location.distance(from: location2)  {
                return true
            }
            else{
                return false
            }
        }
    }
    func savedAirBox() -> DataState {
        guard let saved = UserDefaults.getAirQualityData() else { return DataState.notFound }
        if Date().timeIntervalSince(saved.timeStamp) > kExpireTime {
            return DataState.find(data: saved.data, hasExpired: true)
        }
        else{
            return DataState.find(data: saved.data, hasExpired: false)
        }
    }
    func fetchAirBoxData(sortWith location: CLLocation? = nil, needToSave: Bool = false, completion: @escaping ((_ response: Result<[AirBox]>) -> Void))  {
        apiService.getAllAirBoxList() { (response) in
            switch response.result {
            case .success(let responseData):
                
                DispatchQueue.global().async { [unowned self] in
                    guard let rawData = responseData as? [String: Any] else { return }
                    guard let rawDeviesData = rawData["devices"] as? [[String: Any]] else { return }
                    var parsedData = self.parseAirBoxEdimax(rawDeviesData)
                    if let kLocation = location{
                        self.sortNearest(boxes: &parsedData, with: kLocation)
                    }
                    if needToSave{
                        UserDefaults.save(parsedData)
                    }
                    DispatchQueue.main.async {
                        completion(Result.success(parsedData))
                    }
                }
            case .failure(let error):
                DispatchQueue.main.async {
                    completion(Result.failure(error))
                }
            }
        }
    }
}
