//
//  APIManager.swift
//  AirQuality
//
//  Created by 吳成恩 on 2018/7/5.
//  Copyright © 2018年 吳成恩. All rights reserved.
//

import UIKit
import Alamofire

protocol APIProtocol {
    func getAllAirBoxList(completion: @escaping ((DataResponse<Any>)->()))
}

class APIManager: APIProtocol{
    static let shared = APIManager()
    private let manger: SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 30.0
        return SessionManager(configuration: configuration)
    }()
    
    func getAllAirBoxList(completion: @escaping ((DataResponse<Any>)->())) {
        manger.request(kAPIAllAirBoxEdimax).responseJSON { (response) in
            completion(response)
        }
    }
}
