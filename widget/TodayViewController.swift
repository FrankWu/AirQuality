//
//  TodayViewController.swift
//  Widget
//
//  Created by 吳成恩 on 2018/7/17.
//  Copyright © 2018年 吳成恩. All rights reserved.
//

import UIKit
import NotificationCenter
import CoreLocation

import Alamofire

fileprivate let kExpireTime = TimeInterval(600.0)

class TodayViewController: UIViewController, NCWidgetProviding {
        
    @IBOutlet weak var loadingLabel: UILabel!
    
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var amountView: AmountView!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var tempertureLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    let locationManager = CLLocationManager()
    enum State: Equatable{
        case loading
        case normal(_ data: AirBox?)
        case error(_ error: Error)
        static func == (lhs: State, rhs: State) -> Bool{
            switch (lhs, rhs) {
            case (.loading, .loading):
                return true
            case (.normal(_), .normal(_)):
                return true
            case (.error(_), .error(_)):
                return true
            default:
                return false
            }
        }
    }
    var state: State = .loading {
        willSet{
            if state == .loading && newValue != .loading{
                loadingLabel.isHidden = true
                container.isHidden = false
            }
            else if state != .loading && newValue == .loading{
                loadingLabel.isHidden = false
                container.isHidden = true
            }
        }
        didSet{
            switch state {
            case .normal(let airBox):
                guard var airBox = airBox else { return }
                addressLabel.text = airBox.name
                amountView.configurate(amount: Int(airBox.pm25), color: airBox.quality.color)
                timeLabel.text = "更新時間:" + airBox.formattedTime
                statusLabel.text = airBox.quality.status
                statusLabel.textColor = airBox.quality.color
                tempertureLabel.text = String(Int(airBox.temperature))
                humidityLabel.text = String(Int(airBox.humidity))
                amountView.animate()
            case .loading: break
                
            default: break
            }
        }
    }
    
    deinit {
        print("deinit")
    }
    override func didReceiveMemoryWarning() {
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        initVariable()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        switch CLLocationManager.authorizationStatus() {
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
        case .denied, .restricted:
            locationManager.requestWhenInUseAuthorization()
        default:
            locationManager.requestLocation()
        }
    }

    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        print("Widget", #function)
        switch AirQualityManager.shared.savedAirBox(){
        case .notFound:
            break;
            //fetchingAirBoxData()
        case .find(var data, let expired):
            if let location = locationManager.location {
                AirQualityManager.shared.sortNearest(boxes: &data, with: location)
            }
            state = .normal(data.first)
            if expired{
                //fetchingAirBoxData()
            }
        }
        // Perform any setup necessary in order to update the view.
        // If an error is encountered, use NCUpdateResult.Failed
        // If there's no update required, use NCUpdateResult.NoData
        // If there's an update, use NCUpdateResult.NewData
        completionHandler(NCUpdateResult.newData)
    }
    func widgetActiveDisplayModeDidChange(_ activeDisplayMode: NCWidgetDisplayMode, withMaximumSize maxSize: CGSize) {
        DispatchQueue.main.async {
            if activeDisplayMode == .compact {
                self.preferredContentSize = CGSize(width: 0, height: 110)
            }
            else{
                self.preferredContentSize = CGSize(width: 0, height: 180)
            }
        }
    }
    //MARK: Private
    func initVariable() {
        extensionContext?.widgetLargestAvailableDisplayMode = .compact
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
    }
    func showLocationAuthorizationAlert() {
        let alertVC = UIAlertController(title: "系統訊息", message: "尚未打開位置授權，是否前往設定？", preferredStyle: .alert)
        let goAction = UIAlertAction(title: "前往", style: .default) { (action) in
            if let settingsURL = URL(string: UIApplication.openSettingsURLString + Bundle.main.bundleIdentifier!) {
    
            }
        }
        let cancelAction = UIAlertAction(title: "取消", style: .cancel)
        alertVC.addAction(goAction)
        alertVC.addAction(cancelAction)
        self.present(alertVC, animated: true)
    }
    // MARK: API
    func fetchingAirBoxData() {
        state = .loading
        AirQualityManager.shared.fetchAirBoxData(sortWith: locationManager.location) { (result) in
            switch result {
            case .success(let data):
                self.state = .normal(data.first)
            case .failure(let error):
                self.state = .error(error)
            }
        }
    }
}

extension TodayViewController: CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else { return }
        switch AirQualityManager.shared.savedAirBox() {
        case .find(var data, _):
            AirQualityManager.shared.sortNearest(boxes: &data, with: location)
            state = .normal(data.first)
        default: break
        }
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
    }
}
